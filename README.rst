Medusa Nodes documentation - procedural hair creation addon for Blender
================================================================================

Medusa Nodes is Geometry Nodes based procedural hair creation addon with custom-made Geometry Nodes Deformers. 

The deformers are streamlined by the help of Medusa Nodes' own hierarchy panel,
where one can easily view and edit all of the different deformers and their parameters.


The addon is in the works scines 2021 Augest and is still under development.
It is a long term project and it will continue exploring new ways of proceduralism in 3d hairs field.
