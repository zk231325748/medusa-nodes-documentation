Installation
==============

.. _installation:


The **Medusa Nodes** addon can be installed just as every other Blender addon, no extra steps included:

1. Go to the *Edit > Preferences > Add-ons*.
2. Click on *Install* button in the upper right corner of the Preferences window.

.. figure:: images/installation_001.JPG
   :width: 400
   :figwidth: 100%
   :align: left

3. Navigate to the directory where the *medusaNodes-vX.X.zip* file is located and select it,
   then click on *Install Add-on* button in the lower right of the file browser window.

.. figure:: images/installation_002.JPG
   :width: 400
   :figwidth: 100%
   :align: left

4. After the addon appears in the *Preferences* window, simply enable it by cheking the box on the left side of the addon name.
   
.. figure:: images/installation_003.JPG
   :width: 400
   :figwidth: 100%
   :align: left