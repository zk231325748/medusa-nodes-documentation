Introduction
=============

.. _introduction:


Workflow
=========

*Medusa Nodes* uses similar workflows like many other industry standard procedural hair creation tool sets,
but with some additional Blender-esque unique features, which are only possible
thanks to Blenders powerful Geometry Nodes system.


Object Types
------------------

There are 3 different kinds of objects. Which are relevant for *Medusa Nodes'* procedural workflow. 

* **Hair**
* **Guides**
* **Emitter**

Hair
   the actual hairs - splines with a certain amount of control points. This is the object which receives all of the deformations and is supposed to be rendered.

Guide
   internally almost the same as *Hair* except it acts like the parent for the *Hair* object and deforms it by using the Guide deformer. *Guide* defines the *Hair's* flow direction. 
   In the language of Blender Particle Hair: The *Guide* is the parent particle and the *Hair* is the interpolated child particle.

Emitter
   the geometry which is used by *Hair* to generate the hairs from. It should have a proper UVMap

.. figure:: images/structure.JPG
   :width: 400
   :figwidth: 100%
   :align: center







