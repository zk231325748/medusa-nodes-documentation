.. figure:: images/hero1024.jpg
   :width: 100%
   :align: center
   


Welcome to Medusa Nodes Documentation
=======================================


**Medusa Nodes** is procedural hair creation addon for Blender.

It feautures Geometry Nodes node groups, which are purposly made to achieve realistic deformations and visualisation of hair.

The node groups are streamlined by the help of Medusa Nodes' own hierarchy panel, 
where one can easily view, and edit all of the different nodes and their parameters. 

Check out the :doc:`about` section for further information, including
how to :doc:`install <installation>` the addon.

.. note::

   The addon and the documentation is under active development.

Contents
--------

.. toctree::
   :maxdepth: 3

   about
   installation
   introduction
   nodes
   


   
