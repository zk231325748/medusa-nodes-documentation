Nodes
=====

.. _nodes:



.. image:: /images/add_deformers_menu.jpg
  :width: 200
  :alt: add_deformers_menu



Generator
------------

.. list-table::
   :widths: 25 50

   * - .. centered:: :doc:`nodes/generator`
     - Generates hairs on mesh object.

.. toctree::
   :hidden:
   :maxdepth: 1

   nodes/generator

Deformers
------------


.. list-table::
   :widths: 25 80
   

   * - .. centered:: :doc:`nodes/guide`
     - Guides the hairs with the help of Guide Curves.
   * - .. centered:: :doc:`nodes/clump`
     - Creates varios kind of clumps, supports nested-clumping.
   * - .. centered:: :doc:`nodes/noise`
     - Adds noise based deformation.
   * - .. centered:: :doc:`nodes/curl`
     - Adds curls to hair.
   * - .. centered:: :doc:`nodes/braid`
     - Adds regular and fishbone braids.
   * - .. centered:: :doc:`nodes/children`
     - Randomly adds simple copies of already existing hairs.
   * - .. centered:: :doc:`nodes/trim`
     - Trims / cuts the hair.

.. toctree::
   :hidden:
   :maxdepth: 1

   nodes/guide
   nodes/clump
   nodes/noise
   nodes/curl
   nodes/braid
   nodes/children
   nodes/trim
     
Visualizer
------------

.. list-table::
   :widths: 25 70
   

   * - .. centered:: :doc:`nodes/visualizer`
     - | Visualizes the hair by controlling the thickness and
       | material assignation and varios ather parameters.


.. toctree::
   :hidden:
   :maxdepth: 1

   nodes/visualizer



Mask Groups
------------


.. list-table::
   :widths: 25 80
   

   * - .. centered:: :doc:`nodes/maskGroups/texture`
     - Uses painted textures as influence mask.
   * - .. centered:: :doc:`nodes/maskGroups/random`
     - Uses random function as influence mask.
   * - .. centered:: :doc:`nodes/maskGroups/percent`
     - Uses percent function as influence mask.
   * - .. centered:: :doc:`nodes/maskGroups/noise`
     - Uses noise function a influence mask.
   * - .. centered:: :doc:`nodes/maskGroups/curve`
     - Uses Float Curve as influence mask.
   * - .. centered:: :doc:`nodes/maskGroups/ramp`
     - Uses Color Ramp as influence mask.

.. toctree::
   :hidden:
   :maxdepth: 1

   nodes/maskGroups/texture
   nodes/maskGroups/random
   nodes/maskGroups/percent
   nodes/maskGroups/noise
   nodes/maskGroups/curve
   nodes/maskGroups/ramp

     