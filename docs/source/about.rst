About
=====

.. _about:

The *Medusa Nodes* project started in 2021 after initial `Fields and Anonymous Attributes Proposal <https://devtalk.blender.org/t/fields-and-anonymous-attributes-proposal/19450>`_ appeared online.



History
---------

First there were experimenting phase, the results were really not that impressive.
Geometry Nodes' development kept progressing and so did the experimenting with procedural hair creation.

Things got little serious after the first `Clump Modifier <https://blenderartists.org/t/hairs-geometry-nodes>`_. It had sub-clumping feature but was painfully slow:

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/NdGG9Jw2SV4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>





